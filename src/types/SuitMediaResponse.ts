export interface LinksType {
  first: string;
  last: string;
  prev: string | null;
  next: string | null;
}

interface LinksItemType {
  url: string | null;
  label: string;
  active: boolean;
}

export interface MetaType {
  current_page: number;
  from: number;
  last_page: number;
  links: LinksItemType[];
  path: string;
  per_page: number;
  to: number;
  total: number;
}

export type SuitMediaResponse<T> = {
  data: T[]
  links: LinksType;
  meta: MetaType
}
