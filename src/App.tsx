import { ParallaxProvider } from "react-scroll-parallax"
import { Header, Banner } from "./components"
import { IdeaLists } from "./features/Ideas"

function App() {
  return (
    <ParallaxProvider>
      <Header />
      <Banner />
      <IdeaLists />
    </ParallaxProvider>
  )
}

export default App
