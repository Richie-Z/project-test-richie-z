import { SuitMediaResponse } from "@/types/SuitMediaResponse"
import axios, { AxiosResponse } from "axios"
import { IdeaDTO, IdeaType } from "."

const DEFAULT_APPEND = { append: ['small_image', 'medium_image'] }
const api = axios.create({
  baseURL: 'https://suitmedia-backend.suitdev.com/',
})
export const getIdeas = async (params: IdeaDTO): Promise<AxiosResponse<SuitMediaResponse<IdeaType>>> => await api.get('/api/ideas', { params: { ...params, ...DEFAULT_APPEND } })
