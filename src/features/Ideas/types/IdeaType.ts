interface ImageType {
  id: number;
  mime: string;
  file_name: string;
  url: string;
}

export interface IdeaType {
  id: number;
  slug: string;
  title: string;
  content: string;
  published_at: string;
  deleted_at: string | null;
  created_at: string;
  updated_at: string;
  small_image?: ImageType[];
  medium_image?: ImageType[];
}
