export const pageSize = [10, 30, 50] as const
export const sort = ['published_at', '-published_at'] as const

export type IdeaDTO = {
  'page[number]': number,
  'page[size]': typeof pageSize[number],
  sort: typeof sort[number]
}
