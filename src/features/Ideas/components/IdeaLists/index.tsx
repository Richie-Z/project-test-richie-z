import React, { useEffect, useState } from "react"
import { Card } from ".."
import { IdeaDTO, IdeaType, getIdeas, pageSize, sort } from "../.."
import './style.css'
import DoubleLeftIcon from '@/assets/icons/angle-double-left-svgrepo-com.svg?react'
import DoubleRightIcon from '@/assets/icons/angle-double-right-svgrepo-com.svg?react'
import LeftIcon from '@/assets/icons/angle-left-svgrepo-com.svg?react'
import RightIcon from '@/assets/icons/angle-right-svgrepo-com.svg?react'
import { MetaType } from "@/types/SuitMediaResponse"
import generatePagination from "@/utils/generatePagination"

export const IdeaLists = () => {
  const [currentPage, setCurrentPage] = useState<number>(Number(localStorage.getItem('current_page') ?? 1))
  const [perPage, setPerPage] = useState<number>(Number(localStorage.getItem('per_page') ?? pageSize[0]))
  const [sortBy, setSortBy] = useState<string>(localStorage.getItem('sort_by') ?? sort[0])

  const [datas, setDatas] = useState<IdeaType[]>([])
  const [meta, setMeta] = useState<MetaType>()

  const [firstLoad, setFirstLoad] = useState<boolean>(true)

  const fetchData = async () => {
    const params: IdeaDTO = {
      'page[number]': currentPage,
      'page[size]': perPage as typeof pageSize[number],
      sort: sortBy as typeof sort[number]
    }
    try {
      const { data: { data, meta, links } } = await getIdeas(params)
      setDatas(data)
      setMeta(meta)
      console.log(data, meta, links)
      setCurrentPage(meta.current_page)
    } catch (err) {
      console.error(err)
    } finally {
      setFirstLoad(false)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  useEffect(() => {
    if (firstLoad) return
    fetchData()
    localStorage.setItem('current_page', currentPage.toString())
  }, [currentPage])

  useEffect(() => {
    if (firstLoad) return
    fetchData()
    localStorage.setItem('per_page', perPage.toString())
  }, [perPage])

  useEffect(() => {
    if (firstLoad) return
    fetchData()
    localStorage.setItem('sort_by', sortBy)
  }, [sortBy])

  const handlePerPageChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setPerPage(Number(e.target.value))
  }

  const handleSortByChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSortBy(e.target.value)
  }

  const handleFirstPage = () => {
    if (currentPage === 1) return
    setCurrentPage(1)
  }

  const handlePrevPage = () => {
    if (currentPage - 1 === 0) return
    setCurrentPage(currentPage - 1)
  }

  const handleNextPage = () => {
    if (currentPage + 1 === meta?.last_page) return
    setCurrentPage(currentPage + 1)
  }

  const handleLastPage = () => {
    if (currentPage === meta?.last_page) return
    setCurrentPage(meta?.last_page ?? 1)
  }
  return (
    <section className="mx-32 my-20">
      <div className="settings flex justify-between">
        <p className="info">{`Showing ${meta?.from ?? 1} - ${meta?.to ?? 1} of ${meta?.total ?? 0}`}</p>
        <div className="filter flex gap-8">
          <div className="page">
            <label htmlFor="per-page">Show per page:</label>
            <select name="per-page" id="per-page" onChange={handlePerPageChange}>
              {pageSize.map((val, i) => (
                <option value={val} key={i} selected={perPage === val}>{val}</option>
              ))}
            </select>
          </div>
          <div className="sort">
            <label htmlFor="sort-by">Sort by:</label>
            <select name="sort-by" id="sort-by" onChange={handleSortByChange}>
              {sort.map((val, i) => (
                <option value={val} key={i} selected={sortBy === val}>{i ? 'Oldest' : 'Newest'}</option>
              ))}
            </select>
          </div>
        </div>
      </div >

      <div className="items my-12 flex gap-10 flex-wrap justify-center">

        {datas.length === 0 ? (
          <h1>No Data!</h1>
        ) : datas.map((data, i) => (
          <Card key={i} idea={data} />
        ))}
      </div>

      <div className="pagination flex justify-center items-center pt-20">
        <DoubleLeftIcon className={currentPage === 1 ? 'disabled' : ''} onClick={handleFirstPage} />
        <LeftIcon className={currentPage - 1 === 0 ? 'disabled' : ''} onClick={handlePrevPage} />

        {generatePagination({ currentPage, totalPages: meta?.last_page ?? 28, maxPagesToShow: 5 }).map((page, i) => (
          <button key={i} className={currentPage === page ? 'active' : ''} onClick={() => setCurrentPage(page)}>{page}</button>
        ))}

        <RightIcon className={currentPage + 1 === meta?.last_page ? 'disabled' : ''} onClick={handleNextPage} />
        <DoubleRightIcon className={currentPage === meta?.last_page ? 'disabled' : ''} onClick={handleLastPage} />
      </div>
    </section >
  )
}
