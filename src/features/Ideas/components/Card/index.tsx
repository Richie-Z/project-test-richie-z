import { FC } from "react"
import { IdeaType } from "../.."
import dateConverter from "@/utils/dateConverter"
import { LazyLoadImage } from "react-lazy-load-image-component";
import PlaceholderImage from "@/assets/images/placeholder.png"

export const Card: FC<{
  idea: IdeaType
}> = ({ idea }) => {
  const theImage = idea.medium_image ? idea.medium_image[0] : null;
  return (
    <div className="rounded-lg shadow-[0px_0px_10px_0px_rgba(0,0,0,0.3)] max-w-64">
      <LazyLoadImage
        src={theImage?.url ?? 'https://st4.depositphotos.com/14953852/24787/v/450/depositphotos_247872612-stock-illustration-no-image-available-icon-vector.jpg'}
        placeholderSrc={PlaceholderImage}
        alt="theImage?.file_name ?? 'No Image Available'"
        className="rounded-t-lg min-h-48 max-h-48 min-w-64 object-cover"
      />
      <div className="p-6">
        <p className="date text-xs text-gray-400 font-semibold uppercase font-roboto">{dateConverter(idea.created_at)}</p>
        <p className="title font-semibold line-clamp-3 font-roboto">{idea.title}</p>
      </div>
    </div>
  )
}
