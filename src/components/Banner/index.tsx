import BannerImg from '@/assets/images/banner.png'
import './style.css'
import { useParallax } from 'react-scroll-parallax';

export const Banner = () => {
  const parallax = useParallax<HTMLDivElement>({
    scale: [0.7, 1.5, 'easeInQuad'],
  });
  return (
    <section id="banner" className="">
      <div className="banner-1 bg-cover bg-no-repeat" style={{ backgroundImage: `url(${BannerImg})` }}>
        <div className="content text-white m-auto text-center py-40" ref={parallax.ref}>
          <h1 className="text-6xl">Ideas</h1>
          <p className='text-md mt-1'>Where all our great things begin</p>
        </div>
      </div>
    </section>
  )
}
