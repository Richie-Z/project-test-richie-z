import SuitMediaLogo from '@/assets/images/suitmedia.png'
import './style.css'
import { useState } from 'react'

export const Header = () => {
  const menus = [
    'Work',
    'About',
    'Services',
    'Ideas',
    'Carees',
    'Contact'
  ]
  const [menuActive, setMenuActive] = useState<number>(menus.findIndex(menu => menu === 'Ideas'));

  return (
    <header className='flex bg-orange-500 justify-between px-28 py-4 items-center'>
      <div className="imgBx">
        <img src={SuitMediaLogo} alt="Suitmedia logo" className='w-1/2' />
      </div>
      <div className="menus">
        <ul className='flex gap-8 text-white'>
          {menus.map((menu, i) => (
            <li key={i} className={menuActive === i ? 'active' : ''} onClick={() => setMenuActive(i)}>{menu}</li>
          ))}
        </ul>
      </div>
    </header>
  )
}
