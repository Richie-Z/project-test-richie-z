interface PaginationOptions {
  currentPage: number;
  totalPages: number;
  maxPagesToShow: number;
}

export default function generatePagination({
  currentPage,
  totalPages,
  maxPagesToShow,
}: PaginationOptions): number[] {
  const halfMaxPagesToShow = Math.floor(maxPagesToShow / 2);
  let startPage = Math.max(1, currentPage - halfMaxPagesToShow);
  let endPage = Math.min(totalPages, startPage + maxPagesToShow - 1);

  if (endPage - startPage + 1 < maxPagesToShow) {
    startPage = Math.max(1, endPage - maxPagesToShow + 1);
  }

  return Array.from({ length: endPage - startPage + 1 }, (_, index) => startPage + index);
}
