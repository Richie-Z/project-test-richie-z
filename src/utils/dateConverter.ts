import { format, parseISO } from 'date-fns';
import { id } from 'date-fns/locale';

const dateConverter = (date: string) => {
  const inputDate = parseISO(date);
  const formattedDate = format(inputDate, 'dd MMMM yyyy', { locale: id });
  return formattedDate;
}

export default dateConverter
